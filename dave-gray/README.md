# Dave Gray Tutorial

## Progress

- Video number: Video 4
- Time: 18:48

## Starter

1. Tailwindcss init

    ```bash
    npx tailwindcss init
    ```

## Sources

- [Dave Gray youtube playlist](https://www.youtube.com/playlist?list=PL0Zuz27SZ-6M8znNpim8dRiICRrP5HPft).
- [Dave Gray Github](https://github.com/gitdagray/tailwind-css-course).
