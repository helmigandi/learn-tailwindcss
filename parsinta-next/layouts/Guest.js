import React from "react";
import Head from "next/head";
import Link from "next/link";
import ApplicationLogo from "../components/ApplicationLogo";

export default function Guest({ header, title, cardClassName, children }) {
  return (
    <div className="flex items-center justify-center antialiased tracking-tighter text-gray-800 md:bg-gray-100 md:min-h-screen">
      <Head>
        <title>{title}</title>
      </Head>
      <div
        className={`${
          cardClassName ? cardClassName : "lg:w-1/3"
        } w-full md:w-2/3`}
      >
        <ApplicationLogo />
        <div className="overflow-hidden bg-white md:border md:shadow-sm md:rounded-2xl">
          <div className="px-6 py-2 border-b bg-gray-50/50">
            <h1 className="font-medium capitalize">{header}</h1>
          </div>
          <div className="px-6 pt-3 pb-6">{children}</div>
        </div>
      </div>
    </div>
  );
}
