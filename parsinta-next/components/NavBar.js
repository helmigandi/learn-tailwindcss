import Link from "next/link";
import React from "react";
import { Menu, Transition } from "@headlessui/react";
import NavLink from "./NavLink";
import DropdownLink from "./DropdownLink";
import Line from "./Line";

export default function NavBar() {
  const auth = {
    check: false,
    user: {
      name: "Bambang Sanusi",
    },
  };

  return (
    <div className="hidden py-4 border-b md:block border-white/10">
      <div className="container">
        <nav className="flex items-center justify-between">
          <div className="flex items-center gap-x-2">
            <Link href="/" className="mr-6 font-semibold text-white uppercase">
              Brand
            </Link>
            <NavLink href="/">Home</NavLink>
            <NavLink href="/articles">Articles</NavLink>
            <NavLink href="#">Gallery</NavLink>
            <NavLink href="#">About</NavLink>
            <NavLink href="#">Contact</NavLink>
          </div>
          {auth.check ? (
            <div className="flex items-center gap-x-2">
              <Menu className="relative" as={"div"}>
                <Menu.Button className="flex items-center text-white hover:bg-transparent gap-x-2">
                  {auth.user.name}
                  {/* prettier-ignore */}
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
              </svg>
                </Menu.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items
                    as="div"
                    className={
                      "shadow-sm border absolute top-0 right-0 w-56 py-1 mt-8 overflow-hidden bg-white rounded"
                    }
                  >
                    <DropdownLink href={"#"}>Account settings</DropdownLink>
                    <DropdownLink href={"#"}>Dashboard</DropdownLink>
                    <Line />
                    <DropdownLink href={"#"}>Logout</DropdownLink>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          ) : (
            <div className="flex items-center gap-x-2">
              <NavLink href="/login">Sign in</NavLink>
              <NavLink href="/register">Sign up</NavLink>
            </div>
          )}
        </nav>
      </div>
    </div>
  );
}
