import React from "react";

export default function Button({ children, className, ...props }) {
  return (
    <button
      {...props}
      className={`${
        className
          ? className
          : "focus:ring-indigo-200  bg-indigo-500  hover:bg-indigo-600"
      } border border-transparent px-4 py-2 text-sm font-medium transition duration-300 focus:outline-none focus:ring rounded-xl text-white shadow`}
    >
      {children}
    </button>
  );
}
