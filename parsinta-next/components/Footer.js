import React from "react";
import ApplicationLogo from "./ApplicationLogo";

export default function Footer() {
  return (
    <footer className="mt-8 border-t">
      <div className="container">
        <div className="pt-5 -mb-3 text-center">
          <ApplicationLogo />
        </div>
        <div className="flex justify-center py-5">
          <div className="w-full max-w-lg text-center">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem
            quam, recusandae quis dolorum veritatis nulla, nemo consequuntur
            temporibus quisquam expedita libero est facere ipsum laboriosam
            eveniet itaque. Rem, dicta temporibus.
          </div>
        </div>
      </div>
      <div className="py-2.5 text-center bg-gray-100 border-t border-gray-200 text-sm">
        <div className="container">&copy; Copyright 2022</div>
      </div>
    </footer>
  );
}
