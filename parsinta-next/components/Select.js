import React from "react";

export default function Select({ children, ...props }) {
  return (
    <select
      {...props}
      className="w-full transition duration-200 border-gray-300 rounded-lg shadow-sm focus:ring focus:ring-indigo-200 focus:border-indigo-400"
    >
      {children}
    </select>
  );
}
