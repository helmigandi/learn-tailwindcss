import React from "react";
import MobileNav from "./MobileNav";
import NavBar from "./NavBar";

const Title = ({ children, className }) => (
  <h1
    className={` ${
      className ?? className
    } text-xl font-bold text-white md:text-3xl inline-block`}
  >
    {children}
  </h1>
);

const Body = ({ children }) => (
  <div className="py-4 text-white sm:py-8 md:py-16">{children}</div>
);

const Hero = ({ children }) => {
  return (
    <div className="mb-4 sm:mb-8 bg-gradient-to-br from-gray-900 via-slate-800 to-gray-900">
      <MobileNav />
      <NavBar />
      <div className="container">
        <div className="w-full md:w-2/3">{children}</div>
      </div>
    </div>
  );
};

Hero.Title = Title;
Hero.Body = Body;
export default Hero;
