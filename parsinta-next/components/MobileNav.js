import React, { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import MobileNavLink from "./MobileNavLink";
import Line from "./Line";

export default function MobileNav() {
  const auth = {
    check: true,
    user: {
      name: "Bambang Sanusi",
    },
  };

  return (
    <Menu
      as={"div"}
      className="w-full flex md:hidden items-center justify-between px-4 py-2.5 border-b border-gray-700"
    >
      <a href="#" className="font-semibold text-white uppercase">
        Brand
      </a>
      <Menu.Button className="focus:outline-none">
        {/* prettier-ignore */}
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 text-white">
          <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
        </svg>
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items
          as="div"
          className={
            "shadow-sm border absolute top-0 right-0 w-56 py-1 mt-3 mr-12 overflow-hidden bg-white rounded"
          }
        >
          <MobileNavLink href="#">Home</MobileNavLink>
          <MobileNavLink href="#">Articles</MobileNavLink>
          <MobileNavLink href="#">Gallery</MobileNavLink>
          <MobileNavLink href="#">About</MobileNavLink>
          <MobileNavLink href="#">Contact</MobileNavLink>
          {auth.check ? (
            <>
              <MobileNavLink href="#">{auth.user.name}</MobileNavLink>
              <Line />
              <MobileNavLink href="#">Account settings</MobileNavLink>
              <MobileNavLink href="#">Dashboard</MobileNavLink>
              <Line />
              <MobileNavLink href="#">Logout</MobileNavLink>
            </>
          ) : (
            <>
              <Line />
              <MobileNavLink href="#">Sign in</MobileNavLink>
              <MobileNavLink href="#">Sign up</MobileNavLink>
            </>
          )}
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
