import React from "react";

export default function Checkbox({ label, forInput, ...props }) {
  return (
    <div className="flex items-center gap-x-1">
      <input
        className="text-indigo-500 border-gray-300 rounded"
        type="checkbox"
        {...props}
      />
      <label htmlFor={forInput} className="select-none">
        {label}
      </label>
    </div>
  );
}
