import React from "react";
import { Menu } from "@headlessui/react";

export default function DropdownLink({ href, children }) {
  return (
    <Menu.Item>
      <a
        href={href}
        className="block px-4 py-2 text-sm text-gray-700 cursor-pointer hover:bg-gray-100 hover:text-black"
      >
        {children}
      </a>
    </Menu.Item>
  );
}
