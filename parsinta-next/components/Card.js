import React from "react";

function Card() {
  return (
    // Card
    <div className="max-w-lg">
      <div className="overflow-hidden bg-white border shadow-sm rounded-xl">
        <header className="border-b px-4 py-2.5 bg-gray-50/50 flex items-center justify-between">
          <div>
            <h1 className="font-medium">Card title</h1>
            <span className="text-gray-500">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </span>
          </div>
          <a
            href="#"
            className="px-3 py-2 text-sm font-semibold text-white transition duration-300 bg-pink-500 rounded-md shadow-sm focus:ring focus: ring-pink-200 hover:bg-pink-600 focus:outline-none"
          >
            New task
          </a>
        </header>
        <section className="px-4 py-2.5">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus
          quis nulla vulputate, fermentum mi et, sollicitudin felis. Duis ut ex
          volutpat, blandit lectus vitae, molestie enim.
        </section>
        <footer className="border-t px-4 py-2.5 text-gray-500 bg-gray-50/50">
          Lorem ipsum dolor sit amet.
        </footer>
      </div>
    </div>
  );
}

export default Card;
