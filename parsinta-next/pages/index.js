import Hero from "../components/Hero";
import App from "../layouts/App";

export default function index() {
  return (
    <>
      <Hero>
        <Hero.Body>
          <header className="py-4 text-white sm:py-8 md:py-16">
            <Hero.Title
              className={
                "text-transparent bg-gradient-to-r from-blue-500 to-cyan-500 bg-clip-text"
              }
            >
              New Revolution
            </Hero.Title>
            <p className="mt-4 mb-6 text-base font-light leading-relaxed md:text-xl">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.{" "}
              <a
                href="#"
                className="font-medium underline decoration-sky-500 text-sky-400"
              >
                Dolorem quam
              </a>
              , recusandae quis dolorum veritatis nulla, nemo consequuntur
              temporibus quisquam expedita libero est facere ipsum laboriosam
              eveniet itaque. Rem, dicta temporibus?
            </p>
            <a
              href="#"
              className="text-sm md:text-tiny bg-white text-gray-900 px-4 py-2 md:px-6 md:py-2.5 rounded-xl font-medium inline-flex shadow-inner hover:bg-gray-200 hover:text-blue-600 transition duration-300 shadow-gray-300 md:shadow-gray-400"
            >
              Browse
            </a>
          </header>
        </Hero.Body>
      </Hero>
      <div className="container">
        <div className="w-full max-w-xl">
          Oh yeah. It's the best part. It's crunchy, it's explosive, it's where
          the muffin breaks free of the pan and sort of does it's own thing.
          I'll tell you. That's a million dollar idea right there. Just sell the
          tops.
        </div>
      </div>
    </>
  );
}

index.getLayout = (page) => <App title="Tailwind">{page}</App>;
