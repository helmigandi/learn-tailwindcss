import React from "react";
import Button from "../components/Button";
import Checkbox from "../components/Checkbox";
import Input from "../components/Input";
import Label from "../components/Label";
import Guest from "../layouts/Guest";

export default function Login() {
  return (
    <div>
      <form>
        <div className="mb-6">
          <Label forInput="email">Email</Label>
          <Input
            type="email"
            name="email"
            id="email"
            placeholder="example@mail.com"
          />
        </div>
        <div className="mb-6">
          <Label forInput="password">Password</Label>
          <Input type="password" name="password" id="password" />
        </div>
        <div className="flex justify-between mb-5">
          <Checkbox
            forInput={"remember"}
            label={"Remember"}
            name="remember"
            id="remember"
          />
          <a
            className="text-indigo-500 underline decoration-indigo-500"
            href="#"
          >
            Forgot password
          </a>
        </div>
        <Button>Login</Button>
      </form>
    </div>
  );
}

Login.getLayout = (page) => (
  <Guest header="login" title="Login">
    {page}
  </Guest>
);
