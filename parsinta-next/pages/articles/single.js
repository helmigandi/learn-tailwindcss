import Head from "next/head";
import React from "react";
import Hero from "../../components/Hero";
import App from "../../layouts/App";
import ArticleBody from "../../components/ArticleBody";

export default function Single() {
  return (
    <div>
      <Head>
        <title>Test title</title>
      </Head>
      <Hero>
        <Hero.Body>
          <Hero.Title>Test title</Hero.Title>
          <p className="mt-4 mb-6 text-base font-light leading-relaxed md:text-xl">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </p>
        </Hero.Body>
      </Hero>
      <div className="container">
        <div className="max-w-4xl mx-auto">
          <ArticleBody />
        </div>
      </div>
    </div>
  );
}

Single.getLayout = (page) => <App title="Our articles">{page}</App>;
