import Image from "next/image";
import Link from "next/link";
import React from "react";
import Hero from "../../components/Hero";
import App from "../../layouts/App";

export default function Index() {
  const posts = [
    {
      title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      created_at: "03 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1493514789931-586cb221d7a7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80",
    },
    {
      title: "Donec luctus, quam a lobortis fringilla, elit mi finibus neque",
      created_at: "03 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1507090960745-b32f65d3113a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
    },
    {
      title: "Fusce sed risus feugiat, finibus orci eu, euismod nisl",
      created_at: "03 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1576225410873-a28b2a79fd93?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
    },
    {
      title:
        "Donec tincidunt neque nec viverra bibendum. Aliquam vel commodo dui",
      created_at: "02 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1539035104074-dee66086b5e3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80",
    },
    {
      title:
        "Etiam id interdum ligula, imperdiet lacinia lectus. Fusce fringilla",
      created_at: "02 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1521464302861-ce943915d1c3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80",
    },
    {
      title: "Sed libero enim, iaculis non mattis in, blandit non purus",
      created_at: "01 December, 2022",
      author: "bambangtri",
      picture:
        "https://images.unsplash.com/photo-1616348436168-de43ad0db179?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=481&q=80",
    },
  ];

  return (
    <>
      <Hero>
        <Hero.Body>
          <Hero.Title>Our articles</Hero.Title>
          <p className="mt-4 mb-6 text-base font-light leading-relaxed md:text-xl">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. ,
            recusandae quis dolorum veritatis nulla, nemo consequuntur
            temporibus quisquam expedita libero est facere ipsum laboriosam
            eveniet itaque. Rem, dicta temporibus?
          </p>
        </Hero.Body>
      </Hero>
      <div className="container">
        <div className="grid grid-cols-1 gap-y-12 sm:gap-y-16 sm:gap-x-16 sm:grid-cols-2 lg:grid-cols-3">
          {posts.map((post, index) => (
            <div key={index}>
              <Link href={"/articles/single"}>
                <img
                  className="hover:shadow-lg transition-shadow duration-300 object-cover object-center aspect-[4/2.333] mb-2 rounded-md shadow"
                  src={post.picture}
                  alt={post.title}
                />
              </Link>
              <Link href={"/articles/single"}>
                <h2 className="mb-2">{post.title}</h2>
              </Link>
              <div className="flex items-center justify-between font-mono text-sm text-gray-500">
                <span>{post.author}</span>
                <span>{post.created_at}</span>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

Index.getLayout = (page) => <App title="Our articles">{page}</App>;
