# Brad Traversy Udemy Course

## Sources

- [Tailwind From Scratch Course Projects
](https://github.com/bradtraversy/tailwind-course-projects).
- [Simple Tailwind CSS Starter
](https://github.com/bradtraversy/simple-tailwind-starter).
- [Tailwind Sandbox
](https://github.com/bradtraversy/tailwind-sandbox).
